package com.reubro.sample

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class NetworkInformation {

    companion object {
        fun isNetworkAvailable(context: Context) : Boolean {
            val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = connectivity.allNetworkInfo
            for (networkInfo : NetworkInfo in info){
                if(networkInfo.state == NetworkInfo.State.CONNECTED){
                    return true
                }
            }
            return false

        }
    }

}