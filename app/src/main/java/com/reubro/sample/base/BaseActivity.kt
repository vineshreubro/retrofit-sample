package com.reubro.sample.base

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.reubro.sample.NetworkInformation
import com.reubro.sample.R

open class BaseActivity : AppCompatActivity(),MvpView {

    private val dialog: Dialog by lazy {
        Dialog(this)

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun showLoading() {
        dialog.setContentView(R.layout.progress_dialog)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    override fun hideLoading() {
        dialog.cancel()
    }

    override fun showToast(message:String){
        Toast.makeText(applicationContext,message, Toast.LENGTH_SHORT).show()
    }

    override fun netWorkConnected(): Boolean {

        var flag: Boolean

        if (!NetworkInformation.isNetworkAvailable(this)) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            flag = false
        } else {
            flag = true
            println("internet")

        }
        return flag;

    }


    override fun hidekeyboard() {
    }


}