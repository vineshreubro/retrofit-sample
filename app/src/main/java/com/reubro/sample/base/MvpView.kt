package com.reubro.sample.base

interface MvpView {

    fun showLoading()
    fun hideLoading()
    fun hidekeyboard()
    fun showToast(message:String)
    fun netWorkConnected():Boolean

}