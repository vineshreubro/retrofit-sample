package com.reubro.sample.webservice

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST("homeowner")
    fun registrationHomeOwner(@Query("step") step:String,
                              @Query("user_name") username:String,
                              @Query("email") email: String,
                              @Query("password") password: String
    ) : Call<JsonObject>


}