package com.reubro.sample.registration

import android.os.Bundle
import android.view.View
import com.reubro.sample.R
import com.reubro.sample.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),View.OnClickListener,IStepView {


    private lateinit var presenter: StepPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initlialization()
    }

    private fun initlialization(){
        presenter= StepPresenter(this,this)
        submitRegistrationButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.submitRegistrationButton -> {
                presenter.registerData(usernameRegistrationEditText.text.toString(),emailRegistrationEditText.text.toString(),passwordRegistrationEditText.text.toString())
            }
            else -> {

            }
        }
    }

    override fun onEmailError(message: String) {
        println("email"+message)
        emailRegistrationEditText.setText(message)

    }

    override fun onUsernameError(message: String) {
     println("username"+message)
    }

    override fun onPasswordError(message: String) {
        println("pass"+message)
    }

}