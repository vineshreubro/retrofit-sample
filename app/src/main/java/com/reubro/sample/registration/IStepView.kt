package com.reubro.sample.registration

import com.reubro.sample.base.MvpView

interface IStepView:MvpView {

    fun onEmailError(message:String)
    fun onUsernameError(message: String)
    fun onPasswordError(message: String)
}