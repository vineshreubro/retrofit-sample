package com.reubro.sample.registration

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.reubro.sample.R
import com.reubro.sample.model.StepErrorInputErrorItem
import com.reubro.sample.model.StepResponseBase
import com.reubro.sample.model.StepResponseError
import com.reubro.sample.webservice.RetrofitObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Logger

class StepPresenter(var context:Context, var iStepView: IStepView):IStepPresenter {
    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
//
//    override fun registerData(username: String, email: String, password: String) {
//
//        RetrofitObject.instance.registrationHomeOwner("1",username,email,password).
//                enqueue(object: Callback<Response<JsonObject>> {
//                    override fun onResponse(
//                        call: Call<Response<JsonObject>>, response: Response<Response<JsonObject>>
//                    ) {
//
//                        if(response.code()==200){
//                            try {
//                             //   val successResponse=gson.fromJson(response.body().toString(),StepResponseError::class.java)
//
//                                println("success"+successResponse)
//                            }catch (e:Exception){
//                            //    println("response${JSONObject(response.message())}")
//                                e.printStackTrace()
//
//                               // val successError=gson.fromJson(response.body().toString(),StepResponseError::class.java)
//
//                            }
//
//
//
//                        }else{
//                            val errorResponse=gson.fromJson(response.errorBody()!!.charStream(),StepResponseBase::class.java)
//                                if(errorResponse.message==context.getString(R.string.validation)){
//                                    for(i  in 0 until errorResponse.data.errors.size){
//                                        var responseBaseError=errorResponse.data.errors[i]
//                                        if(responseBaseError.input_filed=="email"){
//                                            iStepView.onEmailError(responseBaseError.error_message)
//
//                                        }
//                                    }
//                                }
//
//                        }
//
//
//                    }
//
//                    override fun onFailure(call: Call<Response<JsonObject>>, t: Throwable) {
//
//                    }
//
//                })
//    }

//        override fun registerData(username: String, email: String, password: String) {
//
//        RetrofitObject.instance.registrationHomeOwner("1",username,email,password).
//                enqueue(object:Callback<JsonObject>{
//                    override fun onResponse(
//                        call: Call<JsonObject>,
//                        response: Response<JsonObject>
//                    ) {
//
//                        //println("succes"+successResponse)
//                        println("res${response.code()}")
//
//
//
//                        if(response.code()==200){
//                            var jsonObject=JSONObject(response.body().toString())
//                            try {
//                                val successResponse=gson.fromJson(response.body().toString(),StepResponseBase::class.java)
//                            }catch (e:Exception){
//                                val successResponse=gson.fromJson(response.body().toString(),StepResponseError::class.java)
//
//                                println("succes"+successResponse)
//                         }
//
//                        }else{
//                            val errorResponse=gson.fromJson(response.errorBody()!!.charStream(),StepResponseBase::class.java)
//                                if(errorResponse.message==context.getString(R.string.validation)){
//                                    for(i  in 0 until errorResponse.data.errors.size){
//                                        var responseBaseError=errorResponse.data.errors[i]
//                                        if(responseBaseError.input_filed=="email"){
//                                            iStepView.onEmailError(responseBaseError.error_message)
//
//                                        }
//                                    }
//                                }
//
//                        }
////
//
//
//                    }
//
//                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
//                    }
//
//                })
//    }

    override fun registerData(username: String, email: String, password: String) {
//
        RetrofitObject.instance.registrationHomeOwner("1",username,email,password).
                enqueue(object:Callback<JsonObject>{
                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {

                        //println("succes"+successResponse)
                        println("res${response.code()}")



                        if(response.code()==200){
                            var jsonObject=JSONObject(response.body().toString())
                            try {
                                val successResponse=gson.fromJson(response.body().toString(),StepResponseBase::class.java)
                            }catch (e:Exception){
                                val successResponse=gson.fromJson(response.body().toString(),StepResponseError::class.java)

                                println("succes"+successResponse)
                         }

                        }else{
                            val errorResponse=gson.fromJson(response.errorBody()!!.charStream(),StepResponseBase::class.java)
                                if(errorResponse.message==context.getString(R.string.validation)){
                                    for(i  in 0 until errorResponse.data.errors.size){
                                        var responseBaseError=errorResponse.data.errors[i]
                                        if(responseBaseError.input_filed=="email"){
                                            iStepView.onEmailError(responseBaseError.error_message)

                                        }
                                    }
                                }



                        }
//


                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    }

                })
    }

}