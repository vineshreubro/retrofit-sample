package com.reubro.sample.model

data class StepResponseBase(
    var status:String?=null,
    var message:String?=null,
    var data:StepResponseData
)

data class StepResponseData(
    var general_message:String,
    var errors:ArrayList<StepErrorInputItem>
)

data class StepErrorInputItem(
    var input_filed:String,
    var error_message:String
)