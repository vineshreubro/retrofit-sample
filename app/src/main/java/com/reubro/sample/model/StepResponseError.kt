package com.reubro.sample.model


data class StepResponseError(
    var status:String?=null,
    var message:String?=null,
    var data:StepResponseErrorData?=null
)

data class StepResponseErrorData(
    var general_message:String,
    var errors:StepErrorInputErrorItem
)

data class StepErrorInputErrorItem(
    var input_field:String,
    var error_message:String
)